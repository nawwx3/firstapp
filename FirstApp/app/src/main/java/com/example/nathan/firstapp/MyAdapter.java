package com.example.nathan.firstapp;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    // TODO: changed this from "String[]"
    private ArrayList<String> mDataSet;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // todo: change to private
        private final TextView mtextView;

            // todo: this changed a lot too
        public MyViewHolder(View v) {
            super(v);

            // todo: all of this (to bottom)
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Element " + getAdapterPosition() + " clicked.", Toast.LENGTH_LONG).show();
                    Log.d("MyAdapter", "Element " + getAdapterPosition() + " clicked.");
                }
            });
            mtextView = v.findViewById(R.id.recycler_textView);
        }

        public TextView getTextView() {
            return mtextView;
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param myDataset ArrayAdapter<String> containing the data to populate views to be used by RecyclerView.
     */
    // todo: type change
    public MyAdapter(ArrayList<String> myDataset) {
        mDataSet = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
//  this is the thing that makes the scrolling work
        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        // todo:   mDataSet.get()
        holder.getTextView().setText(mDataSet.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        // todo: mDataSet.size()
        return mDataSet.size();
    }
}