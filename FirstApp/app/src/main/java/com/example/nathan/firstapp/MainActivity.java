package com.example.nathan.firstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

// todo: start here
// https://developer.android.com/training/basics/firstapp/building-ui

public class MainActivity extends AppCompatActivity {
    // todo: extra message
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    // TODO: all this recycler stuff
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    // todo: this
    private ArrayList<String> mItem = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // todo: this
        Button button_intent = findViewById(R.id.button_intent);
        button_intent.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(view.getContext(), DisplayMessageActivity.class);
                EditText editText = findViewById(R.id.enter_message);
                String message = editText.getText().toString();
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });

        // todo: this
        Button button_list = findViewById(R.id.button_list);
        button_list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Code here executes on main thread after user presses button
                EditText enter_drink = findViewById(R.id.enter_drink);
                String drink = enter_drink.getText().toString();

                mItem.add(drink);
                mAdapter.notifyDataSetChanged();
                enter_drink.getText().clear();
            }
        });


//        TODO: all this recycler stuff  -> from website
        mRecyclerView = findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(mItem);
        mRecyclerView.setAdapter(mAdapter);
    }

}
